-- 1: Remove film from Inventory and Corresponding Rental Records
DELETE FROM rental
WHERE inventory_id IN (
    SELECT inventory_id
    FROM inventory
    WHERE film_id = (SELECT film_id FROM film WHERE title = 'Poor Things')
)

DELETE FROM inventory
WHERE film_id = (SELECT film_id FROM film WHERE title = 'Poor Things')

-- 2: Remove records related to me as a customer
DELETE FROM rental
WHERE customer_id = (SELECT customer_id
    FROM customer
    WHERE first_name = 'Matvey'
      AND last_name = 'Kevorkov'
      AND email = 'myemail@example.com')

DELETE FROM payment
WHERE customer_id = (SELECT customer_id
    FROM customer
    WHERE first_name = 'Matvey'
      AND last_name = 'Kevorkov'
      AND email = 'myemail@example.com')

